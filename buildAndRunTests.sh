python3 $RISE2JABULON tests/1-text build/tests/1-text

cd tests/2-triangle/resourcesSrc/shaders/
./compileAndPackShaders.sh
cd ../../../../
python3 $RISE2JABULON tests/2-triangle build/tests/2-triangle

cd tests/3-two-triangles/resourcesSrc/shaders/
./compileAndPackShaders.sh
cd ../../../../
python3 $RISE2JABULON tests/3-two-triangles build/tests/3-two-triangles

cd tests/4-texture/resourcesSrc/shaders/
./compileAndPackShaders.sh
cd ../../../../
python3 $RISE2JABULON tests/4-texture build/tests/4-texture