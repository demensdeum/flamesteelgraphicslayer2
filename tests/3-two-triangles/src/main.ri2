!raw

#define WIDTH 1024
#define HEIGHT 768

bx::Thread thread;
int counter = 0;
SDL_SysWMinfo wmi;

static bx::DefaultAllocator s_allocator;
static bx::SpScUnboundedQueue s_apiThreadEvents(&s_allocator);

static auto applicationTitle = "Flame Steel Graphics Layer 2 - Two Triangles Test";

bgfx::ShaderHandle loadShader(const char *filename)
{
    FILE *file = fopen(filename, "rb");
    fseek(file, 0, SEEK_END);
    long fileSize = ftell(file);
    fseek(file, 0, SEEK_SET);

    const bgfx::Memory *mem = bgfx::alloc(fileSize + 1);
    fread(mem->data, 1, fileSize, file);
    mem->data[mem->size - 1] = '\0';
    fclose(file);

    return bgfx::createShader(mem);        
}

struct PosColorVertex
{
    float x;
    float y;
    float z;
    uint32_t abgr;
};

enum class EventType
{
	Exit,
	Key,
	Resize
};

struct ExitEvent
{
	EventType type = EventType::Exit;
};

int32_t threadMain(bx::Thread *self, void *userData)
{
    bgfx::Init init;

	init.platformData.nwh  = (void*)(uintptr_t)wmi.info.x11.window;
	init.platformData.ndt  = wmi.info.x11.display;
	init.resolution.width  = WIDTH;
	init.resolution.height = HEIGHT;
	init.resolution.reset  = BGFX_RESET_VSYNC;
    init.type = bgfx::RendererType::OpenGL;

    if (bgfx::init(init) != true) {
        return 1;
    }

    static PosColorVertex vertices[] =
    {
        { -1.0f, -1.0f,  0.0f, 0xFFFF0000 },
        { 1.0f,  -1.0f,  0.0f, 0xFF00FF00 },
        { 1.0f,   1.0f,  0.0f, 0xFF0000FF },
        { 1.0f, 1.0f,  0.0f, 0xFFFF0000 },
        { -1.0f,  1.0f,  0.0f, 0xFF00FF00 },
        { -1.0f,   -1.0f,  0.0f, 0xFF0000FF },
    };

    static const uint16_t indices[] =
    {
        0, 1, 2,
        3, 4, 5
    };

    bgfx::setDebug(BGFX_DEBUG_TEXT);
    bgfx::setViewClear(0, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x303030ff, 1.0f, 0);

    bgfx::VertexLayout vertexLayout;
    vertexLayout.begin()
    .add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
    .add(bgfx::Attrib::Color0, 4, bgfx::AttribType::Uint8, true)
    .end();
    bgfx::VertexBufferHandle vertexBuffer = bgfx::createVertexBuffer(bgfx::makeRef(vertices, sizeof(vertices)), vertexLayout);
    bgfx::IndexBufferHandle indexBuffer = bgfx::createIndexBuffer(bgfx::makeRef(indices, sizeof(indices)));

    bgfx::ShaderHandle vertexShader = loadShader("vertexShader.bin");
    bgfx::ShaderHandle fragmentShader = loadShader("fragmentShader.bin");
    bgfx::ProgramHandle program = bgfx::createProgram(vertexShader, fragmentShader, true);

    bool exit = false;
    while (!exit) {
        while (auto ev = (EventType *)s_apiThreadEvents.pop()) {
            if (*ev == EventType::Exit) {
                exit = true;
            }
        }

        bgfx::setViewRect(0, 0, 0, uint16_t(WIDTH), uint16_t(HEIGHT));
        bgfx::touch(0);
        bgfx::dbgTextClear();
        bgfx::dbgTextPrintf(10, 1, 0x4f, applicationTitle, counter++);

        bgfx::setVertexBuffer(0, vertexBuffer);
        bgfx::setIndexBuffer(indexBuffer);
        bgfx::submit(0, program);

        bgfx::frame();
    }

    bgfx::shutdown();
    return 0;
}

!end

main {

!raw
    SDL_Init(SDL_INIT_VIDEO);
    auto window = SDL_CreateWindow(
        applicationTitle,
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        WIDTH,
        HEIGHT,
        SDL_WINDOW_SHOWN
    );

    SDL_VERSION(&wmi.version);
    if (!SDL_GetWindowWMInfo(window, &wmi)) {
        return false;
    }

    bgfx::renderFrame();    

    thread.init(threadMain);

    bool exit = false;
    SDL_Event event;
    while (!exit) {
        bgfx::renderFrame();
!end
        if counter > 200 {
            exit = true;
            s_apiThreadEvents.push(new ExitEvent);
        }
!raw
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                exit = true;
                s_apiThreadEvents.push(new ExitEvent);                
                break;

            case SDL_WINDOWEVENT: {
                const SDL_WindowEvent& wev = event.window;
                switch (wev.event) {
                case SDL_WINDOWEVENT_CLOSE:
                    exit = true;
                    break;
                }
            } break;
            }
        }
    }

    while (bgfx::RenderFrame::NoContext != bgfx::renderFrame()) {};

    thread.shutdown();

    SDL_DestroyWindow(window);
    SDL_Quit();
!end

    print("Exit success!")

    return 0;
} 
