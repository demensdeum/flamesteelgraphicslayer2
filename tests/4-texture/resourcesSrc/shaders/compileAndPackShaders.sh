rm ../../resources/vertexShader.bin
rm ../../resources/fragmentShader.bin
shaderc -f vertexShader.sc -o vertexShader.bin --type vertex --platform linux
shaderc -f fragmentShader.sc -o fragmentShader.bin --type fragment --platform linux
mv vertexShader.bin ../../resources
mv fragmentShader.bin ../../resources